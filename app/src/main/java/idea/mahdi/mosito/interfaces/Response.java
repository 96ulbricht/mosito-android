package idea.mahdi.mosito.interfaces;

public interface Response<T> {
    void response(T response);
}

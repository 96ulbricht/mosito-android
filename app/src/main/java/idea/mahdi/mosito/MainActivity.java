package idea.mahdi.mosito;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import idea.mahdi.mosito.databinding.ActivityMainBinding;
import idea.mahdi.mosito.resources.Repositories.ClientRep;
import idea.mahdi.mosito.resources.Repositories.UserRep;
import idea.mahdi.mosito.services.DownloadService;
import idea.mahdi.mosito.services.NotificationService;
import idea.mahdi.mosito.services.PlayService;
import idea.mahdi.mosito.services.SyncService;
import idea.mahdi.mosito.ui.auth.AuthActivity;
import idea.mahdi.mosito.ui.browse.BrowseFragment;
import idea.mahdi.mosito.ui.dialogs.ToastDialog;
import idea.mahdi.mosito.ui.library.LibraryFragment;
import idea.mahdi.mosito.ui.player.PlayerFragment;
import idea.mahdi.mosito.ui.search.SearchFragment;
import idea.mahdi.mosito.utils.BaseActivity;
import idea.mahdi.mosito.utils.Utils;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends BaseActivity implements
        FragmentManager.OnBackStackChangedListener,
        BottomNavigationView.OnNavigationItemSelectedListener,
        EasyPermissions.PermissionCallbacks {

    String[] perms;
    UserRep userRep;
    private static final int REQUEST_CODE = 396;
    Activity activity;
    ActivityMainBinding binding;
    BroadcastReceiver toastDialogReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = MainActivity.this;
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_main);
        binding.bottomNavigation.setOnNavigationItemSelectedListener(this);
        setTitle("");

        this.perms = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        this.userRep = new UserRep();

        getSupportFragmentManager().beginTransaction().replace(R.id.framePlayer, new PlayerFragment()).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLibrary, new LibraryFragment()).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.frameBrowse, new BrowseFragment()).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.frameSearch, new SearchFragment()).commit();

        toastDialogReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = intent.getStringExtra("message");
                int drawableId = intent.getIntExtra("drawableId", 0);
                ToastDialog dialog = ToastDialog.newInstance(message, drawableId);
                dialog.show(getSupportFragmentManager(), "");
            }
        };

        startService(new Intent(activity, DownloadService.class));
        startService(new Intent(activity, PlayService.class));
        startService(new Intent(activity, SyncService.class));
        startService(new Intent(activity, NotificationService.class));

        binding.imgProfile.setOnClickListener(v -> startActivity(new Intent(activity, AuthActivity.class)));

        userRep.userLiveData().observe(this, user -> {
            if (user != null && user.getPicture() != null) {
                binding.setUserPicture(user.getPicture());
            }
        });

        binding.imgDownload.setOnClickListener(v -> {
            Intent i = new Intent(activity, DownloadActivity.class);
            startActivity(i);
        });

        registerRemoteConfig();
        checkUpdate();
    }

    private void checkUpdate() {
        ClientRep clientRep = new ClientRep();
        clientRep.checkUpdate(response -> activity.runOnUiThread(() -> {
            binding.imgUpdate.setVisibility(View.VISIBLE);
            binding.imgUpdate.setOnClickListener(v -> {
                Intent i = new Intent(activity, UpdateActivity.class);
                i.putExtra("client", response);
                startActivity(i);
            });
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_option_menu, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_library:
                binding.viewFlipper.setDisplayedChild(0);
                return true;
            case R.id.action_browse:
                binding.viewFlipper.setDisplayedChild(1);
                return true;
            case R.id.action_search:
                binding.viewFlipper.setDisplayedChild(3);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onBackStackChanged() {
        Fragment fragment = getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getFragments().size() - 1);

        if (fragment == null || fragment.getView() == null) return;
        switch (((ViewGroup) fragment.getView().getParent()).getId()) {
            case R.id.frameBrowse:
                binding.viewFlipper.setDisplayedChild(1);
                binding.bottomNavigation.setSelectedItemId(R.id.action_browse);
                return;
            case R.id.frameSearch:
                binding.viewFlipper.setDisplayedChild(3);
                binding.bottomNavigation.setSelectedItemId(R.id.action_search);
                return;
            case R.id.frameLibrary:
                binding.bottomNavigation.setSelectedItemId(R.id.action_library);
                binding.viewFlipper.setDisplayedChild(0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(toastDialogReceiver, new IntentFilter("TOAST_DIALOG"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(toastDialogReceiver);
    }

    public void registerRemoteConfig() {
        FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600).build();

        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);

        Map<String, Object> defaults = new HashMap<>();

        defaults.put("mosito_domain", "mosito.ir");
        defaults.put("mosito_storage", "s1.mosito.ir");

        mFirebaseRemoteConfig.setDefaultsAsync(defaults);

        mFirebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        String mosito_domain = mFirebaseRemoteConfig.getString("mosito_domain");
                        String mosito_storage = mFirebaseRemoteConfig.getString("mosito_storage");

                        Log.e("MainActivity", "mosito_domain: " + mosito_domain);
                        Utils.setShare("mosito_domain", mosito_domain);
                        Application.mosito_domain = mosito_domain;

                        Log.e("MainActivity", "mosito_storage: " + mosito_storage);
                        Utils.setShare("mosito_storage", mosito_storage);
                        Application.mosito_storage = mosito_storage;
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("MainActivity", "OnFailureListener: " + e.getMessage(), e);
                    }
                });
    }

    private void requirePermission() {
        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, "mosito need external storage permission", REQUEST_CODE, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, Arrays.asList(perms))) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }
}

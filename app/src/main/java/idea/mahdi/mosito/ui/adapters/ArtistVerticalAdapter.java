package idea.mahdi.mosito.ui.adapters;

import android.app.Activity;

import idea.mahdi.mosito.R;
import idea.mahdi.mosito.databinding.AdapterArtistsVerticalBinding;
import idea.mahdi.mosito.interfaces.Response;
import idea.mahdi.mosito.models.Artist;
import idea.mahdi.mosito.utils.AdapterUtil;

public class ArtistVerticalAdapter extends AdapterUtil<Artist, AdapterArtistsVerticalBinding> {
    private Response<Artist> onClick;

    public ArtistVerticalAdapter(Activity activity) {
        super(activity, R.layout.adapter_artists_vertical);
    }

    @Override
    public void bindViewHolder(AdapterArtistsVerticalBinding binding, Artist item, int position) {
        binding.setArtist(item);
        binding.getRoot().setOnClickListener(v -> {
            if (onClick != null) onClick.response(item);
        });
    }

    public void setOnClick(Response<Artist> onClick) {
        this.onClick = onClick;
    }
}

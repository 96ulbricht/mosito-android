package idea.mahdi.mosito.resources.Apis;

import idea.mahdi.mosito.models.Album;
import idea.mahdi.mosito.models.Music;
import idea.mahdi.mosito.models.Paginate;
import idea.mahdi.mosito.models.Playlist;
import idea.mahdi.mosito.resources.BaseRetrofit;
import idea.mahdi.mosito.resources.models.ListIds;
import idea.mahdi.mosito.resources.models.SyncRequest;
import idea.mahdi.mosito.resources.models.SyncResponse;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public class LibraryApi extends BaseRetrofit implements LibraryInterface {

    @Override
    public Observable<SyncResponse> index(SyncRequest request) {
        return create(LibraryInterface.class).index(request);
    }

    @Override
    public Observable<Paginate<Music>> musics(ListIds ids) {
        return create(LibraryInterface.class).musics(ids);
    }

    @Override
    public Observable<Paginate<Album>> albums(ListIds ids) {
        return create(LibraryInterface.class).albums(ids);
    }

    @Override
    public Observable<Paginate<Playlist>> playlists(ListIds ids) {
        return create(LibraryInterface.class).playlists(ids);
    }
}

interface LibraryInterface {
    @POST("libraries")
    Observable<SyncResponse> index(@Body SyncRequest request);

    @PUT("libraries/musics")
    Observable<Paginate<Music>> musics(@Body ListIds ids);

    @PUT("libraries/albums")
    Observable<Paginate<Album>> albums(@Body ListIds ids);

    @PUT("libraries/playlists")
    Observable<Paginate<Playlist>> playlists(@Body ListIds ids);
}

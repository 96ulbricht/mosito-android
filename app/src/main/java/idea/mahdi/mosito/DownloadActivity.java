package idea.mahdi.mosito;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import idea.mahdi.mosito.databinding.AdapterMusicDownloadBinding;
import idea.mahdi.mosito.databinding.ActivityDownloadBinding;
import idea.mahdi.mosito.models.MusicDownloadEm;
import idea.mahdi.mosito.resources.Repositories.MusicRep;
import idea.mahdi.mosito.utils.AdapterUtil;

public class DownloadActivity extends AppCompatActivity {
    private MusicRep musicRep;
    private Activity activity;
    private ActivityDownloadBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);


        activity = DownloadActivity.this;
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_download);

        musicRep = new MusicRep();


        LinearLayoutManager manager = new LinearLayoutManager(activity);
        AdapterUtil<MusicDownloadEm, AdapterMusicDownloadBinding> adapterUtil =
                new AdapterUtil<MusicDownloadEm, AdapterMusicDownloadBinding>(activity, R.layout.adapter_music_download) {
                    @Override
                    public void bindViewHolder(AdapterMusicDownloadBinding binding, MusicDownloadEm item, int position) {
                        binding.setMusicDownload(item);
                    }
                };
        binding.recycler.setLayoutManager(manager);
        binding.recycler.setItemAnimator(null);
        binding.recycler.setAdapter(adapterUtil);

        musicRep.getMusicDao().selectDownloads().observe(this, musicDownloadEms -> {
            adapterUtil.setArray(musicDownloadEms);
            binding.tvEmpty.setVisibility(musicDownloadEms.size() == 0 ? View.VISIBLE : View.GONE);
            binding.tvTitle.setVisibility(musicDownloadEms.size() == 0 ? View.GONE : View.VISIBLE);
        });
    }
}
package idea.mahdi.mosito.ui.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.paging.PagedListAdapter;

import java.util.Objects;

import idea.mahdi.mosito.R;
import idea.mahdi.mosito.databinding.AdapterArtistBinding;
import idea.mahdi.mosito.interfaces.Response;
import idea.mahdi.mosito.models.Artist;
import idea.mahdi.mosito.ui.adapters.diffUtil.ArtistLibraryDiffUtil;
import idea.mahdi.mosito.utils.PaginateViewHolder;

public class ArtistPagedAdapter extends PagedListAdapter<Artist, PaginateViewHolder<AdapterArtistBinding>> {
        private Response<Artist> clicked;

        public ArtistPagedAdapter() {
            super(new ArtistLibraryDiffUtil());
            setHasStableIds(true);
        }

        public void setClicked(Response<Artist> clicked) {
            this.clicked = clicked;
        }

        @NonNull
        @Override
        public PaginateViewHolder<AdapterArtistBinding> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new PaginateViewHolder<>(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_artist, parent, false));
        }

        @Override
        public long getItemId(int position) {
            return Objects.requireNonNull(getItem(position)).getId();
        }

        @Override
        public void onBindViewHolder(@NonNull PaginateViewHolder<AdapterArtistBinding> holder, int position) {
            Artist a = getItem(position);
            holder.binding.setArtist(a);
            holder.binding.getRoot().setOnClickListener(view -> clicked.response(a));
        }
    }

package idea.mahdi.mosito.resources.Apis;

import idea.mahdi.mosito.models.Client;
import idea.mahdi.mosito.resources.BaseRetrofit;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ClientApi extends BaseRetrofit implements ClientInterface {

    @Override
    public Observable<Client> index(String platform) {
        return create(ClientInterface.class).index(platform);
    }
}

interface ClientInterface {
    @GET("clients")
    Observable<Client> index(@Query("platform") String platform);
}

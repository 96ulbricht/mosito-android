package idea.mahdi.mosito.ui.auth;

import idea.mahdi.mosito.R;
import idea.mahdi.mosito.databinding.FragmentForgetPasswordBinding;
import idea.mahdi.mosito.utils.FragmentUtil;

public class ForgetPasswordFragment extends FragmentUtil<FragmentForgetPasswordBinding> {
    @Override
    public void onViewCreate() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_forget_password;
    }
}

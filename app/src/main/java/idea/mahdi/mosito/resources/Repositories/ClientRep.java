package idea.mahdi.mosito.resources.Repositories;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import idea.mahdi.mosito.Application;
import idea.mahdi.mosito.interfaces.Response;
import idea.mahdi.mosito.models.Client;
import idea.mahdi.mosito.models.ErrorHandler;
import idea.mahdi.mosito.resources.Apis.ClientApi;
import idea.mahdi.mosito.utils.CallWrapper;
import io.reactivex.schedulers.Schedulers;

public class ClientRep {
    private ClientApi api;

    public ClientRep() {
        api = new ClientApi();
    }

    public void checkUpdate(Response<Client> onResponse) {
        try {
            Application app = Application.getInstance();
            PackageInfo pInfo = app.getPackageManager().getPackageInfo(app.getPackageName(), 0);
            String version = pInfo.versionName;
            Log.e("ClientRep", "Version check : " + version);

            api.index("android").observeOn(Schedulers.io()).subscribe(new CallWrapper<Client>() {
                @Override
                protected void onSuccess(Client client) {
                    int current = Integer.parseInt(version.replace(".", ""));
                    int newest = Integer.parseInt(client.getName().replace("v", "").replace(".", ""));
                    if (current < newest) {
                        Log.e("ClientRep", "Version check : update available -> " + client.getName());
                        onResponse.response(client);
                    } else {
                        Log.e("ClientRep", "Version check : up-to-date");
                    }
                }

                @Override
                protected void onError(int status, ErrorHandler response) {
                    Log.e("ClientRep", response.getMessage());
                }
            });
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("ClientRep", "Version check : " + e.getMessage(), e);
        }
    }
}

package idea.mahdi.mosito.resources;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import idea.mahdi.mosito.converters.AlbumConverter;
import idea.mahdi.mosito.converters.ArtistsConverter;
import idea.mahdi.mosito.converters.MusicConverter;
import idea.mahdi.mosito.converters.MusicsConverter;
import idea.mahdi.mosito.converters.ProfilesConverter;
import idea.mahdi.mosito.converters.StatusConverter;
import idea.mahdi.mosito.converters.UserConverter;
import idea.mahdi.mosito.models.Album;
import idea.mahdi.mosito.models.AlbumMusicRole;
import idea.mahdi.mosito.models.Artist;
import idea.mahdi.mosito.models.ArtistAlbumRole;
import idea.mahdi.mosito.models.ArtistMusicRole;
import idea.mahdi.mosito.models.Music;
import idea.mahdi.mosito.models.MusicDownload;
import idea.mahdi.mosito.models.Playlist;
import idea.mahdi.mosito.models.PlaylistMusicRole;
import idea.mahdi.mosito.models.User;
import idea.mahdi.mosito.models.UserAlbum;
import idea.mahdi.mosito.models.UserMusic;
import idea.mahdi.mosito.models.UserPlaylist;
import idea.mahdi.mosito.resources.Daos.AlbumDao;
import idea.mahdi.mosito.resources.Daos.ArtistDao;
import idea.mahdi.mosito.resources.Daos.MusicDao;
import idea.mahdi.mosito.resources.Daos.PlaylistDao;
import idea.mahdi.mosito.resources.Daos.UserDao;

@TypeConverters({
        ArtistsConverter.class,
        ProfilesConverter.class,
        MusicConverter.class,
        MusicsConverter.class,
        AlbumConverter.class,
        StatusConverter.class,
        UserConverter.class
})
@Database(entities = {
        Artist.class,
        User.class,
        Music.class,
        Album.class,
        UserMusic.class,
        UserAlbum.class,
        ArtistAlbumRole.class,
        ArtistMusicRole.class,
        AlbumMusicRole.class,
        Playlist.class,
        UserPlaylist.class,
        MusicDownload.class,
        PlaylistMusicRole.class}, version = 2, exportSchema = false)
public abstract class BaseRoom extends RoomDatabase {

    public abstract UserDao userDao();

    public abstract MusicDao musicDao();

    public abstract AlbumDao albumDao();

    public abstract ArtistDao artistDao();


    public abstract PlaylistDao playlistDao();

    private static volatile BaseRoom INSTANCE;

    public static BaseRoom getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (BaseRoom.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), BaseRoom.class, "mosito")
                            .fallbackToDestructiveMigration().build();
                }
            }
        }
        return INSTANCE;
    }
}

package idea.mahdi.mosito.ui.adapters;

import android.app.Activity;

import idea.mahdi.mosito.R;
import idea.mahdi.mosito.databinding.AdapterAlbumHorizontalBinding;
import idea.mahdi.mosito.interfaces.FragmentNavigation;
import idea.mahdi.mosito.interfaces.Response;
import idea.mahdi.mosito.models.Album;
import idea.mahdi.mosito.models.AlbumLibrary;
import idea.mahdi.mosito.ui.albums.AlbumFragment;
import idea.mahdi.mosito.ui.artists.ArtistFragment;
import idea.mahdi.mosito.utils.AdapterUtil;

public class AlbumHorizontalAdapter extends AdapterUtil<AlbumLibrary, AdapterAlbumHorizontalBinding> {
    private Response<Album> clicked;

    public AlbumHorizontalAdapter(Activity activity) {
        super(activity, R.layout.adapter_album_horizontal);
    }

    public void setClicked(Response<Album> clicked) {
        this.clicked = clicked;
    }

    @Override
    public void bindViewHolder(AdapterAlbumHorizontalBinding binding, AlbumLibrary item, int position) {
        binding.setAlbum(item.album);
        binding.getRoot().setOnClickListener(v -> {
            if (clicked != null) {
                clicked.response(item.album);
            }
        });
    }
}

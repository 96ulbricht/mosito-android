package idea.mahdi.mosito.ui.adapters;

import android.app.Activity;

import idea.mahdi.mosito.R;
import idea.mahdi.mosito.databinding.AdapterArtistBinding;
import idea.mahdi.mosito.interfaces.Response;
import idea.mahdi.mosito.models.Artist;
import idea.mahdi.mosito.utils.AdapterUtil;

public class ArtistAdapter extends AdapterUtil<Artist, AdapterArtistBinding> {
    private Response<Artist> onClick;

    public ArtistAdapter(Activity activity) {
        super(activity, R.layout.adapter_artist);
    }

    @Override
    public void bindViewHolder(AdapterArtistBinding binding, Artist item, int position) {
        binding.setArtist(item);
        binding.getRoot().setOnClickListener(v -> {
            if (onClick != null) onClick.response(item);
        });
    }

    public void setOnClick(Response<Artist> onClick) {
        this.onClick = onClick;
    }
}

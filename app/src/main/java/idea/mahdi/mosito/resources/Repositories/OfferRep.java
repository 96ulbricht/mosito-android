package idea.mahdi.mosito.resources.Repositories;

import android.util.Log;

import java.util.List;

import idea.mahdi.mosito.interfaces.Response;
import idea.mahdi.mosito.models.ErrorHandler;
import idea.mahdi.mosito.models.Offer;
import idea.mahdi.mosito.resources.Apis.OfferApi;
import idea.mahdi.mosito.utils.CallWrapper;

public class OfferRep {
    private OfferApi api;

    public OfferRep() {
        api = new OfferApi();
    }

    public void offers(Response<List<Offer>> response) {
        api.offers().subscribe(new CallWrapper<List<Offer>>() {
            @Override
            protected void onSuccess(List<Offer> offers) {
                response.response(offers);
            }

            @Override
            protected void onError(int status, ErrorHandler response) {
                Log.e("OfferRep.offers", response.getMessage());
            }
        });
    }
}

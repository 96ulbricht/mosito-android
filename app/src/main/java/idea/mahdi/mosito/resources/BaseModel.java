package idea.mahdi.mosito.resources;

import java.io.Serializable;

public abstract class BaseModel implements Serializable {
    public abstract long getId();
}

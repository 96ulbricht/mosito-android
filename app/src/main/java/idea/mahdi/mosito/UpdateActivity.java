package idea.mahdi.mosito;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import idea.mahdi.mosito.databinding.ActivityUpdateBinding;
import idea.mahdi.mosito.models.Client;

public class UpdateActivity extends AppCompatActivity {

    private Activity activity;
    private ActivityUpdateBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = UpdateActivity.this;
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_update);

        binding.imgBack.setOnClickListener(v -> finish());

        Client client = (Client) getIntent().getSerializableExtra("client");

        if (client == null) {
            finish();
            return;
        }

        binding.setClient(client);

        try {
            Application app = Application.getInstance();
            PackageInfo pInfo = app.getPackageManager().getPackageInfo(app.getPackageName(), 0);
            String version = pInfo.versionName;
            binding.setCurrent(version);
            Log.e("UpdateActivity", "Version check : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("UpdateActivity", "Version check : " + e.getMessage(), e);
        }

        binding.btnDownload.setOnClickListener(v -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://" + Application.mosito_domain + "/fileManager/" + client.getFile_id()));
            startActivity(i);
        });

        binding.tvSource.setOnClickListener(v -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(client.getSource()));
            startActivity(i);
        });
    }
}